
















































// const { Keystone } = require('@keystonejs/keystone')
// const { PasswordAuthStrategy } = require('@keystonejs/auth-password')
// const { Text, Checkbox, Password, Uuid } = require('@keystonejs/fields')
// const { AdminUIApp } = require('@keystonejs/app-admin-ui')
// const initialiseData = require('./initial-data')

// const { KnexAdapter: Adapter } = require('@keystonejs/adapter-knex')
// const PROJECT_NAME = 'drlz'
// const adapterConfig = {
//   knexOptions: {
//     connection: 'postgresql://admin:passworddho@postgres:5432/drlz',
//   },
// }
// const { atTracking } = require('@keystonejs/list-plugins')
// const { byTracking } = require('@keystonejs/list-plugins')
// const uuid = require('uuid')

// const { logging } = require('@keystonejs/list-plugins')

// const keystone = new Keystone({
//   appVersion: {
//     version: '1.0.0',
//     addVersionToHttpHeaders: true,
//     access: true,
//   },

//   cookieSecret: process.env.NODE_ENV === 'production',

//   cookie: {
//     secure: process.env.NODE_ENV === 'production', // Default to true in production
//     maxAge: 1000 * 60 * 60 * 24 * 30, // 30 days
//     sameSite: false,
//   },

//   defaultAccess: {
//     list: true,
//     field: true,
//     custom: true,
//   },

//   adapter: new Adapter(adapterConfig),
//   onConnect: process.env.CREATE_TABLES !== 'true' && initialiseData,
// })

// const userIsAdmin = ({ authentication: { item: user } }) =>
//   Boolean(user && user.isAdmin)
// const userOwnsItem = ({ authentication: { item: user } }) => {
//   if (!user) {
//     return false
//   }
//   return { id: user.id }
// }

// const userIsAdminOrOwner = (auth) => {
//   const isAdmin = access.userIsAdmin(auth)
//   const isOwner = access.userOwnsItem(auth)
//   return isAdmin ? isAdmin : isOwner
// }

// const access = { userIsAdmin, userOwnsItem, userIsAdminOrOwner }

// const { DateTime } = require('@keystonejs/fields')

// keystone.createList('SKU', {
//   fields: {
//     id: { type: Uuid, defaultValue: uuid.v4 },
//     labeller: { type: Text },
//     licenceNumber: { type: Text },
//     inn: { type: Text },
//     atc: { type: Text },
//     titles: { type: Text },
//     form: { type: Text },
//     strenght: { type: Text },
//     primaryPack: { type: Text },
//     primaryQuantity: { type: Text },
//     primaryVolume: { type: Text },
//     SecondaryPack: { type: Text },
//     secondaryQuantity: { type: Text },
//     stringID: {
//       type: Text,
//       adminConfig: {
//         isReadOnly: true, //slug can be created automatically and you may want to show this as read only
//       },
//     },
//     code1: {
//       type: Text,
//       adminConfig: {
//         isReadOnly: true, //slug can be created automatically and you may want to show this as read only
//       },
//     },
//   },
//   plugins: [
//     logging((args) => console.log(args)),
//     atTracking({
//       read: true,
//       create: false,
//       update: false,
//     }),

//     byTracking({
//       read: true,
//       create: false,
//       update: false,
//     }),
//   ],
//   labelField: 'inn',
// })

// keystone.createList('formsVocab', {
//   fields: {
//     id: { type: Uuid, defaultValue: uuid.v4 },
//     formsPackage: { type: Text },
//     numericID: { type: Text },
//   },
//   plugins: [logging((args) => console.log(args))],
// })

// keystone.createList('User', {
//   fields: {
//     name: { type: Text },
//     email: {
//       type: Text,
//       isUnique: true,
//     },
//     isAdmin: {
//       type: Checkbox,
//       access: {
//         update: access.userIsAdmin,
//       },
//     },
//     password: {
//       type: Password,
//     },
//   },
//   access: {
//     read: access.userIsAdmin,
//     update: access.userIsAdmin,
//     create: access.userIsAdmin,
//     delete: access.userIsAdmin,
//     auth: true,
//   },
// })

// const authStrategy = keystone.createAuthStrategy({
//   type: PasswordAuthStrategy,
//   list: 'User',
//   config: {
//     identityField: 'name',
//     secretField: 'password',
//   },
// })

// module.exports = {
//   keystone,
//   apps: [
//     new AdminUIApp({
//       name: PROJECT_NAME,
//       enableDefaultRoute: true,
//       authStrategy,
//     }),
//   ],
// }
