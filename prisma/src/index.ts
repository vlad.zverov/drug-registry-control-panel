import { PrismaClient } from '@prisma/client'
import express from 'express'

const prisma = new PrismaClient()
const app = express()
const version = 'v1.0'

app.use(express.json())

app.get('/users', async (req, res) => {
  try {
    const data = await prisma.user.findMany()
    res.status(200).json(data)
  } catch (error) {
    res.status(401).json({ success: false, message: error.message })
  }
})

app.get('/sku2', async (req, res) => {
  try {
    const data = await prisma.sku2.findMany()
    res.status(200).json(data)
  } catch (error) {
    res.status(401).json({ success: false, message: error.message })
  }
})

app.get('/sku2/:id', async (req, res) => {
  try {
    const { id } = req.params
    const data = await prisma.sku2.findUnique({
      where: { id: Number(id) },
    })
    res.status(200).json(data)
  } catch (error) {
    res.status(401).json({ success: false, message: error.message })
  }
})

app.get('/inn-vocab', async (req, res) => {
  try {
    const data = await prisma.innVocab.findMany()
    res.status(200).json({ version: `Версія словника: ${version}`, data })
  } catch (error) {
    res.status(401).json({ success: false, message: error.message })
  }
})

app.get('/labeller-vocab', async (req, res) => {
  try {
    const data = await prisma.labellerVocab.findMany()
    res.status(200).json({ version: `Версія словника: ${version}`, data })
  } catch (error) {
    res.status(401).json({ success: false, message: error.message })
  }
})

app.get('/sku-vocab', async (req, res) => {
  try {
    const data = await prisma.skuVocab.findMany()
    res.status(200).json({ version: `Версія словника: ${version}`, data })
  } catch (error) {
    res.status(401).json({ success: false, message: error.message })
  }
})

app.get('/forms-vocab', async (req, res) => {
  try {
    const data = await prisma.formsVocab.findMany()
    res.status(200).json({ version: `Версія словника: ${version}`, data })
  } catch (error) {
    res.status(401).json({ success: false, message: error.message })
  }
})

app.get('/primary-vol-vocab', async (req, res) => {
  try {
    const data = await prisma.primaryVolVocab.findMany()
    res.status(200).json({ version: `Версія словника: ${version}`, data })
  } catch (error) {
    res.status(401).json({ success: false, message: error.message })
  }
})

app.get('/primary-pack-vocab', async (req, res) => {
  try {
    const data = await prisma.primaryPackVocab.findMany()
    res.status(200).json({ version: `Версія словника: ${version}`, data })
  } catch (error) {
    res.status(401).json({ success: false, message: error.message })
  }
})

app.get('/secondary-pack-vocab', async (req, res) => {
  try {
    const data = await prisma.secondaryPackVocab.findMany()
    res.status(200).json({ version: `Версія словника: ${version}`, data })
  } catch (error) {
    res.status(401).json({ success: false, message: error.message })
  }
})

app.listen(3001, () =>
  console.log('REST API server ready at: http://localhost:3001')
)
